﻿using ESS.API.DataAccess.Contracts;
using ESS.API.DataAccess.Entities;
using ESS.API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using ESS.Data.Model;

namespace ESS.API.Controllers
{
    [Authorize]
    [Route("api/self")]
    [ApiController]
    public class SelfMonitoringController : ControllerBase
    {
        private readonly RepositorySMContext _context;
        private ILoggerManager _logger;
        private IConfiguration _configuration;
        private UserModel _user;

        public SelfMonitoringController(ILoggerManager logger, RepositorySMContext context, IConfiguration IConfig)
        {
            _logger = logger;
            _context = context;
            _configuration = IConfig;
        }

        [HttpPost("check")]
        public IActionResult CheckIn(SelfMonitoringModel model)
        {
            var response = new ResponseModel();
            _user = UserModel.MappingFromAuth(User);

            #region Validation
            if (string.IsNullOrEmpty(model.SiteCode))
            {
                response.SetError("Lokasi site harus dipilih");
            }

            if (string.IsNullOrEmpty(model.DateTimeCheckInStr))
            {
                response.SetError("Waktu penginputan tidak boleh kosong");
            }
            else
            {
                DateTime dtNow = DateTime.Now;
                if(DateTime.TryParse(model.DateTimeCheckInStr, out dtNow))
                {
                    model.DateTimeCheckIn = dtNow;

                    if (model.DateTimeCheckIn > DateTime.Now.AddHours(1))
                    {
                        response.SetError("Waktu input tidak boleh lebih dari hari ini " + DateTime.Now.AddHours(1).ToString("HH:mm tt") + " (time server)");
                    }

                    if (model.DateTimeCheckIn < DateTime.Now.AddHours(-1))
                    {
                        response.SetError("Waktu input tidak boleh lebih dari hari ini " + DateTime.Now.AddHours(-1).ToString("HH:mm tt") + " (time server)");
                    }
                } else
                {
                    response.SetError("Waktu penginputan tidak valid");
                }
            }

            if (string.IsNullOrEmpty(model.BodyTemperatureStr))
            {
                response.SetError("Suhu tubuh tidak boleh kosong");
            }
            else
            {
                model.BodyTemperature = Decimal.Parse(model.BodyTemperatureStr);
                if (model.BodyTemperature > 41)
                {
                    response.SetError("Suhu tubuh tidak boleh melebihi batas normal 41℃");
                }

                if (model.BodyTemperature < 35)
                {
                    response.SetError("Suhu tubuh tidak boleh kurang dari batas normal 35℃");
                }
            }

            if(model.SaturationO2.HasValue)
            {
                if(model.SaturationO2.Value > 0)
                {
                    if (model.SaturationO2.Value < 70 || model.SaturationO2.Value > 100)
                    {
                        response.SetError("Saturasi O2 ada di angka 70 sampai 100% atau 0 (bagi yang tidak mempunyai alatnya)");
                    }
                }
            }

            if(model.IsVaccine && model.Vaccine != null){

                if (string.IsNullOrEmpty(model.Vaccine.VaccineType))
                {
                    response.SetError("Nama vaksin harus dipilih");
                }

                if (string.IsNullOrEmpty(model.Vaccine.Location))
                {
                    response.SetError("Lokasi vaksin tidak boleh kosong");
                }

                if (string.IsNullOrEmpty(model.Vaccine.VaccineDateStr))
                {
                    response.SetError("Tanggal vaksin tidak boleh kosong");
                }
                else
                {
                    DateTime VaccineDate = DateTime.Now;
                    if(DateTime.TryParse(model.Vaccine.VaccineDateStr, out VaccineDate))
                    {
                        model.Vaccine.VaccineDate = VaccineDate.Date;
                    } else
                    {
                        response.SetError("Waktu penginputan tidak valid");
                    }
                }

            }

            if(model.IsPostVaccine){
                if (model.IsUseMasker)
                {
                    if (string.IsNullOrEmpty(model.MaskerType))
                    {
                        response.SetError("Masker yang digunakan tidak boleh kosong");
                    }
                }
            }
            #endregion

            //model.DateTimeCheckIn = DateTime.Parse(model.DateTimeCheckInStr);
            //model.BodyTemperature = Decimal.Parse(model.BodyTemperatureStr);

            DateTime dateCheckIn = model.DateTimeCheckIn.Date;
            bool IsNight = false;
            if (model.DateTimeCheckIn.Hour >= 17)
            {
                IsNight = true;
            }

            if(response.IsSuccess)
            {
                SelfMonitoring selfMonitor = _context.SelfMonitoring.FirstOrDefault(x => x.NIKSite == _user.NIKSite && x.DateCheckIn == dateCheckIn);
                
                if (selfMonitor != null)
                {
                    #region UPDATE SELFMONITORING
                    if (IsNight)
                    {
                        selfMonitor.BodyTemperatureNight = model.BodyTemperature;
                        selfMonitor.DateTimeNight = model.DateTimeCheckIn;
                    }
                    else
                    {
                        selfMonitor.BodyTemperature = model.BodyTemperature;
                        selfMonitor.DateTimeMorning = model.DateTimeCheckIn;
                    }

                    if (string.IsNullOrEmpty(selfMonitor.Address) && !string.IsNullOrEmpty(model.Address))
                    {
                        selfMonitor.Address = model.Address;
                    }

                    if ((!selfMonitor.Latitude.HasValue) && model.Latitude.HasValue)
                    {
                        selfMonitor.Latitude = model.Latitude;
                    }

                    if ((!selfMonitor.Longitude.HasValue) && model.Longitude.HasValue)
                    {
                        selfMonitor.Longitude = model.Longitude;
                    }

                    selfMonitor.SiteCode = model.SiteCode;
                    selfMonitor.SiteName = model.SiteName;
                    selfMonitor.IsOnDuty = model.IsOnDuty;
                    selfMonitor.IsSoreThroat = model.IsSoreThroat;
                    selfMonitor.IsFlu = model.IsFlu;
                    selfMonitor.IsFever = model.IsFever;
                    selfMonitor.IsCough = model.IsCough;
                    selfMonitor.IsShortness = model.IsShortness;
                    selfMonitor.IsContactPatient = model.IsContactPatient;
                    selfMonitor.IsQuarantine = model.IsQuarantine;
                    selfMonitor.IsSmellDisturbance = model.IsSmellDisturbance;
                    selfMonitor.IsTasteInterference = model.IsTasteInterference;
                    selfMonitor.IsPositiveAntigen = model.IsPositiveAntigen;
                    selfMonitor.IsPositivePCR = model.IsPositivePCR;
                    selfMonitor.IsPositiveOTG = model.IsPositiveOTG;
                    selfMonitor.IsPositiveCOV = model.IsPositiveCOV;
                    selfMonitor.IsDiarrhea = model.IsDiarrhea;
                    selfMonitor.IsSorePain = model.IsSorePain;
                    selfMonitor.IsJointPain = model.IsJointPain;
                    selfMonitor.IsDizzy = model.IsDizzy;
                    selfMonitor.IsHeadache = model.IsHeadache;
                    selfMonitor.IsTired = model.IsTired;
                    selfMonitor.IsShivering = model.IsShivering;
                    selfMonitor.IsNausea = model.IsNausea;
                    selfMonitor.IsGag = model.IsGag;
                    selfMonitor.OtherSymptoms = string.IsNullOrEmpty(model.OtherSymptoms) ? string.Empty : model.OtherSymptoms;

                    if (model.IsVaccine)
                    {
                        selfMonitor.IsUseMasker = model.IsUseMasker;
                        selfMonitor.MaskerType = model.IsUseMasker ? model.MaskerType : string.Empty;
                        selfMonitor.IsKeepDistance = model.IsKeepDistance;
                    }

                    selfMonitor.SaturationO2 = model.SaturationO2.HasValue ? model.SaturationO2.Value : 0;
                    selfMonitor.IsOnDuty = model.IsOnDuty;
                    selfMonitor.IsFromMobile = model.IsFromMobile;
                    selfMonitor.UpdatedDate = DateTime.Now;

                    response.SetSuccess(
                        string.Format("{0} berhasil memperbarui data pada {1} hari", selfMonitor.FullName, (IsNight ? "malam" : "pagi"))
                        );
                    #endregion
                }
                else
                {
                     #region INSERT SELFMONITORING
                    selfMonitor = new SelfMonitoring()
                    {
                        ID = Guid.NewGuid(),
                        DateCheckIn = model.DateTimeCheckIn,
                        NIKSite = _user.NIKSite,
                        IsEmployee = _user.IsHRMS,
                        SiteCode = (!string.IsNullOrEmpty(model.SiteCode) ? model.SiteCode : string.Empty),
                        SiteName = (!string.IsNullOrEmpty(model.SiteName) ? model.SiteName : string.Empty),
                        CompanyName = _user.CompanyName,
                        FullName = _user.FullName,
                        Email = (!string.IsNullOrEmpty(_user.Email) ? _user.Email : string.Empty),
                        Address = (!string.IsNullOrEmpty(model.Address) ? model.Address : string.Empty),
                        PhotoMorning = string.Empty,
                        PhotoNight = string.Empty,
                        Latitude = model.Latitude,
                        Longitude = model.Longitude,
                        IsSoreThroat = model.IsSoreThroat,
                        IsFlu = model.IsFlu,
                        IsFever = model.IsFever,
                        IsCough = model.IsCough,
                        IsShortness = model.IsShortness,
                        IsSmellDisturbance = model.IsSmellDisturbance,
                        IsTasteInterference = model.IsTasteInterference,
                        SaturationO2 = model.SaturationO2.HasValue ? model.SaturationO2.Value : 0,
                        IsContactPatient = model.IsContactPatient,
                        IsQuarantine = model.IsQuarantine,
                        IsFromMobile = model.IsFromMobile,
                        IsOnDuty = model.IsOnDuty,
                        IsPositiveAntigen = model.IsPositiveAntigen,
                        IsPositivePCR = model.IsPositivePCR,
                        IsPositiveOTG = model.IsPositiveOTG,
                        IsPositiveCOV = model.IsPositiveCOV,
                        
                        IsDiarrhea = model.IsDiarrhea,
                        IsSorePain = model.IsSorePain,
                        IsJointPain = model.IsJointPain,
                        IsDizzy = model.IsDizzy,
                        IsHeadache = model.IsHeadache,
                        IsTired = model.IsTired,
                        IsShivering = model.IsShivering,
                        IsNausea = model.IsNausea,
                        IsGag = model.IsGag,

                        OtherSymptoms = string.IsNullOrEmpty(model.OtherSymptoms) ? string.Empty : model.OtherSymptoms,

                        CreatedDate = DateTime.Now,
                        UpdatedDate = DateTime.Now,
                    };

                    if (model.IsVaccine)
                    {
                        selfMonitor.IsUseMasker = model.IsUseMasker;
                        selfMonitor.MaskerType = model.IsUseMasker ? model.MaskerType : string.Empty;
                        selfMonitor.IsKeepDistance = model.IsKeepDistance;
                    }

                    if (IsNight)
                    {
                        selfMonitor.BodyTemperatureNight = model.BodyTemperature;
                        selfMonitor.DateTimeNight = model.DateTimeCheckIn;
                    }
                    else
                    {
                        selfMonitor.BodyTemperature = model.BodyTemperature;
                        selfMonitor.DateTimeMorning = model.DateTimeCheckIn;
                    }

                    _context.SelfMonitoring.Add(selfMonitor);
                    response.SetSuccess(
                        string.Format("{0} berhasil menyimpan pada {1} hari", selfMonitor.FullName, (IsNight ? "malam" : "pagi"))
                        );
                    #endregion
                }

                if (model.IsVaccine)
                {
                    Vaccine vaccine = _context.Vaccine.FirstOrDefault(x => x.NIKSite == _user.NIKSite && x.VaccineDate == model.Vaccine.VaccineDate);

                    if (vaccine == null)
                    {
                        vaccine = new Vaccine();
                        vaccine.NIKSite = _user.NIKSite;
                        vaccine.CreatedDate = DateTime.Now;
                        vaccine.VaccineDate = model.Vaccine.VaccineDate;
                    }

                    vaccine.VaccineType = model.Vaccine.VaccineType;
                    vaccine.Location = model.Vaccine.Location;
                    vaccine.UpdatedDate = DateTime.Now;

                    if(vaccine.ID == Guid.Empty)
                    {
                        vaccine.ID = Guid.NewGuid();
                        _context.Vaccine.Add(vaccine);
                    }
                }

                _context.SaveChanges();
            }

            return Ok(response);
        }

        [HttpGet("list")]
        public IActionResult History(string dateTimeStart)
        {
            _user = UserModel.MappingFromAuth(User);
            DateTime dateStart = DateTime.Now.Date;
            if (!string.IsNullOrEmpty(dateTimeStart))
            {
                var isValid = DateTime.TryParse(dateTimeStart, out dateStart);
                if (!isValid)
                {
                    dateStart = DateTime.Now.Date;
                }
            }

            dateStart = dateStart.Date.AddDays(1).AddMinutes(-1);
            DateTime dateEnd = dateStart.AddDays(-14).Date;

            var source = _context.SelfMonitoring
                .Where(x => x.NIKSite == _user.NIKSite && x.DateCheckIn <= dateStart && x.DateCheckIn >= dateEnd)
                .OrderByDescending(x => x.DateCheckIn)
                .Select(x => new SelfMonitoringViewModel()
                {
                    DateTimeCheckIn = x.DateCheckIn,
                    NIKSite = x.NIKSite,
                    FullName = x.FullName,
                    Photo = (!string.IsNullOrEmpty(x.PhotoNight) ? x.PhotoNight : x.PhotoMorning),
                    SiteName = x.SiteName,
                    CompanyName = x.CompanyName,
                    BodyTemperatureMorning = (x.BodyTemperature.HasValue ? x.BodyTemperature.Value : 0),
                    BodyTemperatureNight = (x.BodyTemperatureNight.HasValue ? x.BodyTemperatureNight.Value : 0),
                    IsSoreThroat = x.IsSoreThroat,
                    IsFlu = x.IsFlu,
                    IsFever = x.IsFever,
                    IsCough = x.IsCough,
                    IsShortness = x.IsShortness,
                    IsSmellDisturbance = x.IsSmellDisturbance,
                    IsTasteInterference = x.IsTasteInterference,
                    SaturationO2 = x.SaturationO2,
                    IsOnDuty = x.IsOnDuty,
                    IsContactPatient = x.IsContactPatient,
                    IsQuarantine = x.IsQuarantine,
                    IsCheckIn = true,
                    IsPositiveAntigen = x.IsPositiveAntigen,
                    IsPositivePCR = x.IsPositivePCR,
                    IsPositiveOTG = x.IsPositiveOTG,
                    IsPositiveCOV = x.IsPositiveCOV,

                    IsDiarrhea = x.IsDiarrhea,
                    IsSorePain = x.IsSorePain,
                    IsJointPain = x.IsJointPain,
                    IsDizzy = x.IsDizzy,
                    IsHeadache = x.IsHeadache,
                    IsTired = x.IsTired,
                    IsShivering = x.IsShivering,
                    IsNausea = x.IsNausea,
                    IsGag = x.IsGag,
                    OtherSymptoms = x.OtherSymptoms,
                    IsUseMasker = x.IsUseMasker.HasValue ? x.IsUseMasker.Value : false,
                    MaskerType = x.MaskerType,
                    IsKeepDistance = x.IsKeepDistance,
                   
                }).ToList();

            List<Vaccine> Vaccines = _context.Vaccine.Where(x => x.NIKSite == _user.NIKSite).ToList();


            List<SelfMonitoringViewModel> _list = new List<SelfMonitoringViewModel>();
            for (int i = 0; i < 14; i++)
            {
                DateTime dtCheckIn = dateStart.AddDays(-i).Date;
                var _item = source.FirstOrDefault(x => x.DateTimeCheckIn.Date == dtCheckIn);
                if (_item == null)
                {
                    _item = new SelfMonitoringViewModel();
                    _item.DateTimeCheckIn = dtCheckIn;
                    _item.IsCheckIn = false;
                }

                if (Vaccines != null && Vaccines.Count > 0)
                {
                    var vaccine = Vaccines.FirstOrDefault(x => x.VaccineDate.Date == _item.DateTimeCheckIn.Date);
                    if (vaccine != null)
                        _item.IsPostVaccine = true;
                }

                _list.Add(_item);
            }

            return Ok(_list);
        }

        [HttpGet("vaccine")]
        public IActionResult Vaccine()
        {
            List<VaccineModel> list = new List<VaccineModel>();
            _user = UserModel.MappingFromAuth(User);
            var vaccines = _context.Vaccine.Where(x => x.NIKSite == _user.NIKSite).ToList();

            if(vaccines != null && vaccines.Count > 0)
            {
                list = vaccines.Select(x => new VaccineModel()
                {
                    ID = x.ID,
                    VaccineDate = x.VaccineDate,
                    VaccineDateStr = x.VaccineDate.ToString("yyyy-MM-dd"),
                    VaccineType = x.VaccineType,
                    Location = x.Location,
                    OnObservation = (x.VaccineDate.Date >= DateTime.Now.AddDays(-7).Date && x.VaccineDate.Date <= DateTime.Now.Date)
                }).ToList();
            }

            return Ok(list);
        }
    }
}