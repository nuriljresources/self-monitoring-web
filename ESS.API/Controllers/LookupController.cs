﻿using ESS.API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using ESS.Data.Model;
using ESS.API.DataAccess.Entities;
using System.Data;

namespace ESS.API.Controllers
{
    [Route("api/lookup")]
    public class LookupController : ControllerBase
    {
        private readonly RepositoryContext _context;
        private readonly IConfiguration Configuration;

        public LookupController(RepositoryContext context, IConfiguration configuration)
        {
            _context = context;
            Configuration = configuration;
        }

        [HttpGet("company-vendor")]
        public IActionResult CompanyVendor()
        {
            List<SelectItemModel> _list = new List<SelectItemModel>();

            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                string SQLQuery = @"SELECT vendor, nmVenLS FROM V_HRMS_LSPOS";

                command.CommandText = SQLQuery;
                command.CommandType = CommandType.Text;

                if (command.Connection.State != ConnectionState.Open)
                {
                    command.Connection.Open();
                }

                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        _list.Add(new SelectItemModel()
                        {
                            Value = reader.GetString(0),
                            Text = reader.GetString(1)
                        });
                    }

                    _list.Add(new SelectItemModel()
                    {
                        Value = "00",
                        Text = "Others"
                    });
                }

                reader.Close();
            }

            return Ok(_list);
        }

        [HttpGet("department")]
        public IActionResult Department()
        {
            List<SelectItemGroupModel> _list = new List<SelectItemGroupModel>();

            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                string SQLQuery = @"SELECT ISNULL(kdsite, 'ALL') kdsite, KdDepar, NmDepar FROM h_a130 WHERE KdDepar != '000' AND StEdit != 2 AND KdDepar IN (SELECT KdDepar FROM H_A101 WHERE Active = 1) ORDER BY kdsite";

                command.CommandText = SQLQuery;
                command.CommandType = CommandType.Text;

                if (command.Connection.State != ConnectionState.Open)
                {
                    command.Connection.Open();
                }

                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        _list.Add(new SelectItemGroupModel()
                        {
                            GroupName = reader.GetString(0),
                            Value = reader.GetString(1),
                            Text = reader.GetString(2)
                        });
                    }
                }

                reader.Close();
            }

            return Ok(_list);
        }


        [HttpGet("vaccine-type")]
        public IActionResult VaccineType()
        {
            List<SelectItemModel> _list = new List<SelectItemModel>();
            var vaccines = Configuration["AppSettings:Vaccines"];

            _list.Add(
                new SelectItemGroupModel()
                {
                    GroupName = string.Empty,
                    Value = "",
                    Text = "Select Vaksin"
                }
            );

            var vaccinesArray = vaccines.Split("|");
            foreach (var vaccine in vaccinesArray)
            {
                _list.Add(
                    new SelectItemGroupModel()
                    {
                        GroupName = string.Empty,
                        Value = vaccine,
                        Text = vaccine
                    }
                );
            }

            return Ok(_list);
        }
    }
}
