﻿using ESS.API.DataAccess.Contracts;
using ESS.API.DataAccess.Entities;
using ESS.API.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Data;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System;
using Microsoft.AspNetCore.Authorization;

namespace ESS.API.Controllers
{
   
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly RepositoryContext _context;
        private readonly RepositorySMContext _contextSM;
        private ILoggerManager _logger;
        private IConfiguration _configuration;

        public UserController(ILoggerManager logger, RepositoryContext context, RepositorySMContext contextSM, IConfiguration IConfig)
        {
            _logger = logger;
            _context = context;
            _contextSM = contextSM;
            _configuration = IConfig;
        }

        [HttpGet("sites")]
        [ResponseCache(Duration = 30)]
        public IActionResult Sites()
        {
            List<SelectItemModel> _list = new List<SelectItemModel>();

            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                string SQLQuery =
                    @"SELECT DISTINCT a.kdSite, a.nmSite 
                        FROM h_a120 a WITH(nolock) 
                        INNER JOIN  H_A110 cp WITH(nolock) 
                        ON cp.kdcompany = a.kdcompany 
                        WHERE
                            a.stedit != '2' AND a.kdCompany IN ('JRN', 'JRBM', 'SPP', 'ASA', 'GSM', 'SRSB', 'MBC')";

                command.CommandText = SQLQuery;
                command.CommandType = CommandType.Text;

                if (command.Connection.State != ConnectionState.Open)
                {
                    command.Connection.Open();
                }

                var reader = command.ExecuteReader();
                _list.Add(new SelectItemModel()
                {
                    Value = "",
                    Text = "Select Site Location"
                });

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        _list.Add(new SelectItemModel()
                        {
                            Value = reader.GetString(0),
                            Text = reader.GetString(1)
                        });
                    }
                }

                reader.Close();
            }

            return Ok(_list);
        }

        [HttpGet("sites-pica")]
        [ResponseCache(Duration = 30)]
        public IActionResult SitesPICA()
        {
            List<SelectItemModel> _list = new List<SelectItemModel>();
            _list.Add(new SelectItemModel()
            {
                Value = "",
                Text = "Select Site Location"
            });

            var sites = _contextSM.Company.ToList();
            foreach (var site in sites)
            {
                _list.Add(new SelectItemModel()
                {
                    Value = site.CompanyCode,
                    Text = site.CompanyName
                });
            }

            return Ok(_list);
        }

        [HttpGet("province")]
        [ResponseCache(Duration = 30)]
        public IActionResult Province()
        {
            List<SelectItemModel> _list = new List<SelectItemModel>();
            _list.Add(new SelectItemModel()
            {
                IsSelected = true,
                Value = "",
                Text = "Select Site Province"
            });

            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                string SQLQuery = @"SELECT KdProv, NmProv FROM R_A015 WHERE KdProv != '00' AND stedit <> '2' ORDER BY NmProv";

                command.CommandText = SQLQuery;
                command.CommandType = CommandType.Text;

                if (command.Connection.State != ConnectionState.Open)
                {
                    command.Connection.Open();
                }

                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        _list.Add(new SelectItemModel()
                        {
                            Value = reader.GetString(0),
                            Text = reader.GetString(1)
                        });
                    }
                }

                reader.Close();
            }

            

            return Ok(_list);
        }

        [HttpGet("area-province")]
        public IActionResult AreaProvince(string KdProv)
        {
            List<SelectItemModel> _list = new List<SelectItemModel>();
            _list.Add(new SelectItemModel()
            {
                IsSelected = true,
                Value = "",
                Text = "Select Area"
            });

            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                string SQLQuery = String.Format(@"SELECT KdKota, NmKota FROM R_A014 WHERE KdProv = '{0}' AND StEdit <> '2' ORDER BY NmKota", KdProv);

                command.CommandText = SQLQuery;
                command.CommandType = CommandType.Text;

                if (command.Connection.State != ConnectionState.Open)
                {
                    command.Connection.Open();
                }

                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        _list.Add(new SelectItemModel()
                        {
                            Value = reader.GetString(0),
                            Text = reader.GetString(1)
                        });
                    }
                }

                reader.Close();
            }

            return Ok(_list);
        }

        [HttpGet("birthday")]
        [ResponseCache(Duration = 30)]
        public IActionResult BirthDay()
        {
            List<BirthDayModel> Models = new List<BirthDayModel>();

            try
            {

                using (var command = _context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "sp_wp_getbirthday";
                    command.CommandType = CommandType.StoredProcedure;

                    if (command.Connection.State != ConnectionState.Open)
                    {
                        command.Connection.Open();
                    }

                    var reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            BirthDayModel _birthDay = new BirthDayModel()
                            {
                                name = reader.GetString(0),
                                dept = reader.GetString(1),
                                date = reader.GetString(2),
                                loc = reader.GetString(3),
                            };

                            Models.Add(_birthDay);
                        }
                    }
                    reader.Close();
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(Models);
        }
    }
}