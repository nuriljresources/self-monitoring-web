﻿using ESS.API.DataAccess.Contracts;
using ESS.API.DataAccess.Entities;

namespace ESS.API.DataAccess.Repository
{
    public class RepositoryWrapper : IRepositoryWrapper 
    { 
        private RepositoryContext _repoContext;
        private IHotSeatRepository _hotSeat; 

        public IHotSeatRepository HotSeat
        { 
            get 
            { 
                if (_hotSeat == null) 
                { 
                    _hotSeat = new HotSeatRepository(_repoContext); 
                } 
                return _hotSeat; 
            } 
        } 

        
        public RepositoryWrapper(RepositoryContext repositoryContext) 
        { 
            _repoContext = repositoryContext; 
        } 
        
        public void Save() 
        {
            _repoContext.SaveChanges();
        } 
    }
}
