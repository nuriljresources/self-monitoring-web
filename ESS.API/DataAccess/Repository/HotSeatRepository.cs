﻿using ESS.API.DataAccess.Contracts;
using ESS.API.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ESS.API.DataAccess.Repository
{
    public class HotSeatRepository : RepositoryBase<HotSeat>, IHotSeatRepository 
    { 
        public HotSeatRepository(RepositoryContext repositoryContext) 
            : base(repositoryContext) 
        { 
        }
    }
}
