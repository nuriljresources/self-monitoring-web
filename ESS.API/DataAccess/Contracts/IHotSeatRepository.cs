﻿using ESS.API.DataAccess.Entities;
using System;
using System.Collections.Generic;

namespace ESS.API.DataAccess.Contracts
{
    public interface IHotSeatRepository : IRepositoryBase<HotSeat>
    {
    }
}
