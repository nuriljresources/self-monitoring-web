﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESS.API.DataAccess.Entities
{
    public class HotSeat
    {
        public Guid Id { get; set; }
        public string BookingId { get; set; }
        public string NIKSite { get; set; } 
        public string FullName { get; set; } 
        public string Email { get; set; } 
        public string Department { get; set; } 
        public string Role { get; set; } 
        public DateTime DateBooking { get; set; } 
        public string Suite { get; set; } 
        public int SeatNo { get; set; } 
        public string Status { get; set; } 
        public string Reason { get; set; } 
        public DateTime DateCreated { get; set; }
    }
}
