﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESS.API.DataAccess.Entities
{
    public class Company
    {
        public Guid ID { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyName { get; set; }
        public string ADCode { get; set; }
        public string AuthEmail { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
