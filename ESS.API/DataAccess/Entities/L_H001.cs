﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESS.API.DataAccess.Entities
{
    public class L_H001
    {
        public L_H001()
        {
            CreatedTime = DateTime.Now;
        }

        public string transid { get; set; }
        public string nik { get; set; }
        public string trans_date { get; set; }
        public string kddepar { get; set; }
        public string kdjabatan { get; set; }
        public string datefrom { get; set; }
        public string dateto { get; set; }
        public decimal? totleave { get; set; }
        public decimal? remainleave { get; set; }
        public decimal? eligiLeave { get; set; }
        public decimal? cutabsent { get; set; }
        public string typeLeave { get; set; }
        public string paidltype { get; set; }
        public string remarks { get; set; }
        public string pic_nik { get; set; }
        public string pic_jabat { get; set; }
        public string verify_nik { get; set; }
        public string verify_jabat { get; set; }
        public string approve_nik { get; set; }
        public string approve_jabat { get; set; }
        public byte? fstatus { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedIn { get; set; }
        public DateTime? CreatedTime { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedIn { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public string StEdit { get; set; }
        public string DeleteBy { get; set; }
        public DateTime? DeleteTime { get; set; }
        public string kdsite { get; set; }
        public decimal? yeara { get; set; }
        public decimal? yearb { get; set; }
        public decimal? aTot { get; set; }
        public decimal? bTot { get; set; }
        public decimal? holiday { get; set; }
        public string ref_fieldbreak { get; set; }
        public string StatusWF { get; set; }
        public string NxtActionr { get; set; }
        public string NxtActionrRole { get; set; }
        public int? fpic_nik { get; set; }
        public int? fapprove_nik { get; set; }
        public int? fverify_nik { get; set; }
        public int? ItemId { get; set; }
    }

}
