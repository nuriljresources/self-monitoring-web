﻿using Microsoft.EntityFrameworkCore;

namespace ESS.API.DataAccess.Entities
{
    public class RepositorySMContext : DbContext
    {
        public RepositorySMContext(DbContextOptions<RepositorySMContext> options) : base(options) { }
        public DbSet<SelfMonitoring> SelfMonitoring { get; set; }
        public DbSet<Vaccine> Vaccine { get; set; }
        public DbSet<UserLogin> UserLogin { get; set; }
        public DbSet<Company> Company { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}