using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESS.API.DataAccess.Entities
{
    public class Vaccine
    {
        public Guid ID { get; set; }
        public string NIKSite { get; set; }
        public string VaccineType { get; set; }
        public DateTime VaccineDate { get; set; }
        public string Location { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
