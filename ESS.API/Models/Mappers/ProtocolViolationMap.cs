﻿using CsvHelper.Configuration;
using ESS.API.Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESS.API.Models.Mappers
{
    public sealed class ProtocolViolationMap : ClassMap<ProtocolViolationModel>
    {
        public ProtocolViolationMap()
        {
            Map(m => m.No).Name(ProtocolViolationConstants.No);
            Map(m => m.Site).Name(ProtocolViolationConstants.Site);
            Map(m => m.NIKSite).Name(ProtocolViolationConstants.NIKSite);
            Map(m => m.Name).Name(ProtocolViolationConstants.NamaPekerja);
            Map(m => m.Position).Name(ProtocolViolationConstants.Jabatan);
            Map(m => m.Departement).Name(ProtocolViolationConstants.Departement);
            Map(m => m.DateIn).Name(ProtocolViolationConstants.DateViolation);
            Map(m => m.IsComorbid).Name(ProtocolViolationConstants.IsComorbid);
            Map(m => m.IsCheckInNight).Name(ProtocolViolationConstants.IsCheckInNight);
            Map(m => m.IsContactPatient).Name(ProtocolViolationConstants.IsContactPatient);
            Map(m => m.IsQuarantine).Name(ProtocolViolationConstants.IsQuarantine);
            Map(m => m.HotSeatStatus).Name(ProtocolViolationConstants.HotSeatStatus);
            Map(m => m.SelfMonitoring).Name(ProtocolViolationConstants.SelfMonitoring);
            Map(m => m.Pelanggaran).Name(ProtocolViolationConstants.Pelanggaran);
            Map(m => m.Tindakan).Name(ProtocolViolationConstants.Tindakan);
        }
    }
}
