﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESS.API.Models
{
    public class HotSeatReportModel
    {
        public string id { get; set; }
        public HotSeatUserModel user { get; set; }
        public DateTime date { get; set; }
        public DateTime createdDate { get; set; }
        public string suiteId { get; set; }
        public HotSeatSuiteModel suite { get; set; }
        public int seatNo { get; set; }
        public string status { get; set; }
        public string statusLabel { get; set; }
        public string reason { get; set; }
        public string dateFormat { get; set; }
    }
    public class HotSeatUserModel
    {
        public string nikSite { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string department { get; set; }
        public string role { get; set; }
    }

    public class HotSeatSuiteModel
    {
        public string title { get; set; }
        public int row { get; set; }
        public int column { get; set; }
    }
}
