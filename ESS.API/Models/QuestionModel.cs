﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESS.API.Models
{
    class QUESTION
    {
        public static string FIRST_WORK_DATE = "FIRST_WORK_DATE";
        public static string DATE_BIRTH = "DATE_BIRTH";
        public static string PLACE_BIRTH = "PLACE_BIRTH";
    }

    public class QuestionModel
    {
        public string Code { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class QuestionRequestModel
    {
        public string Phone { get; set; }
        public string Code { get; set; }
        public string Value { get; set; }
    }
}
