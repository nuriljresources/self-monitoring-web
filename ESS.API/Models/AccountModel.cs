﻿using System;

namespace ESS.API.Models
{
    public class AccountModel
    {
        public Guid Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string AccountType { get; set; }
    }
}
