using System;

namespace ESS.API.Models
{
    public class DayOffModel
    {
        public string ID { get; set; }
        public decimal TotalRemain { get; set; }
        public DateTime ExpiredDate { get; set; }
    }
}
