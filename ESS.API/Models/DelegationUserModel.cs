﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESS.API.Models
{
    public class DelegationUserModel
    {
        public string typex { get; set; }
        public string NIKSITE { get; set; }
        public string KdSite { get; set; }
        public string Nama { get; set; }
        public string KdJabatan { get; set; }
        public string nmJabat { get; set; }
        public string KdDepar { get; set; }
        public string nmdepar { get; set; }
        public string nik { get; set; }
    }
}
