﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESS.API.Models
{
    public class LeaveBalanceModel
    {
        public int Year { get; set; }

        public decimal LeaveEligible { get; set; }
        public decimal LeaveTaken { get; set; }
        public decimal LeaveRemaining { get; set; }
        public decimal CarryForwardRemaining { get; set; }

        public string EligibleDate { get; set; }
        public string ExpiredDate { get; set; }


        public decimal LeaveEligibleLong { get; set; }
        public decimal LeaveTakenLong { get; set; }
        public decimal LeaveRemainingLong { get; set; }
        public decimal CarryForwardRemainingLong { get; set; }

        public string EligibleDateLong { get; set; }
        public string ExpiredDateLong { get; set; }
    }
}
