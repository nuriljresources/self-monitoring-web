namespace ESS.API.Models.Constants
{
    public static class ProtocolViolationConstants
    {
        public const string No = "No";
        public const string Site = "Site";
        public const string NIKSite = "NIKSite";
        public const string NamaPekerja = "Nama Pekerja";
        public const string Jabatan = "Jabatan";
        public const string Departement = "Departement";
        public const string DateViolation = "Tanggal Pelanggaran(DD/MM/YY)";
        public const string IsComorbid = "IsComorbid";
        public const string IsCheckInNight = "IsCheckInNight";
        public const string IsContactPatient = "IsContactPatient";
        public const string IsQuarantine = "IsQuarantine";
        public const string HotSeatStatus = "HotSeat Status";
        public const string SelfMonitoring = "SelfMonitoring Status";
        public const string Pelanggaran = "Pelanggaran";
        public const string Tindakan = "Tindakan";
    }
}
