﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESS.API.Models
{
    public class SelectItemModel
    {
        public string Value { get; set; }
        public string Text { get; set; }
        public bool IsSelected { get; set; }        
    }

    public class SelectItemGroupModel : SelectItemModel
    {
        public string GroupName { get; set; }
    }
}
