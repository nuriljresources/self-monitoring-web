﻿using IdentityServer.DataAccess.Contracts;
using IdentityServer.DataAccess.Entities;
using IdentityServer.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Data;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System;
using ESS.Data.Model;
using System.DirectoryServices;
using IdentityServer.Extensions;
using EmailService;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace IdentityServer.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly RepositoryContext _context;
        private readonly RepositorySMContext _contextSM;
        private ILoggerManager _logger;
        private IConfiguration _configuration;
        private readonly IEmailSender _emailSender;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public AuthController(
            ILoggerManager logger, 
            RepositoryContext context, 
            RepositorySMContext contextSM, 
            IConfiguration IConfig, 
            IEmailSender emailSender,
            IWebHostEnvironment webHostEnvironment)
        {
            _logger = logger;
            _context = context;
            _contextSM = contextSM;
            _configuration = IConfig;
            _emailSender = emailSender;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpPost]
        [Route("login")]
        public IActionResult Login(UserModel model)
        {
            var response = new ResponseModel(false);
            if (model.IsHRMS)
            {
                using (var command = _context.Database.GetDbConnection().CreateCommand())
                {
                    string SQLQuery = string.Format(
                        @"SELECT TOP 1 
                                NIKSITE, 
                                Nama, 
                                NmCompany, 
                                nmDepar, 
                                nmJabat, 
                                kdSite, 
                                ISNULL(Email, '') AS Email 
                            FROM V_HRMS_EMP 
                            WHERE 
                                NIKSite = '{0}' 
                                AND active = 1 
                        UNION ALL 
                        SELECT TOP 1
                                V_HRMS_LSEMP.NIK, 
                                V_HRMS_LSEMP.Nama, 
                                ISNULL(V_HRMS_LSPOS.nmVenLS, '') AS NmCompany, 
                                '' AS nmDepar, 
                                ISNULL(V_HRMS_LSVENDOR.nmjabat, '') AS nmJabat, 
                                V_HRMS_LSEMP.kdSite, 
                                ISNULL(V_HRMS_LSEMP.Email, '') AS Email 
                            FROM 
                                V_HRMS_LSEMP 
                                LEFT JOIN V_HRMS_LSPOS ON V_HRMS_LSEMP.vendor = V_HRMS_LSPOS.vendor 
                                LEFT JOIN V_HRMS_LSVENDOR ON V_HRMS_LSEMP.kdjabatan = V_HRMS_LSVENDOR.Kdjabat 
                            WHERE 
                                V_HRMS_LSEMP.NIK = '{0}' AND 
                                V_HRMS_LSEMP.active = 1", model.NIKSite);

                    command.CommandText = SQLQuery;
                    command.CommandType = CommandType.Text;

                    if (command.Connection.State != ConnectionState.Open)
                    {
                        command.Connection.Open();
                    }

                    var reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            model.NIKSite = reader.GetString(0);
                            model.FullName = reader.GetString(1);
                            model.CompanyName = reader.GetString(2);
                            model.DepartmentName = reader.GetString(3);
                            model.Role = reader.GetString(4);
                            model.SiteCode = reader.GetString(5);

                            model.Email = reader.GetString(6);
                            response.SetSuccess();
                        }
                    } else
                    {
                        response.SetError("NIK {0} not found", model.NIKSite);
                    }
                    reader.Close();
                }
            }
            else
            {
                if(!string.IsNullOrEmpty(model.NIKSite) && !string.IsNullOrEmpty(model.FullName) && !string.IsNullOrEmpty(model.CompanyName))
                {
                    model.NIKSite = model.NIKSite;
                    model.FullName = model.FullName;
                    model.CompanyName = model.CompanyName;
                    model.DepartmentName = string.Empty;
                    model.Role = string.Empty;
                    model.IsHRMS = false;
                    model.SiteCode = string.Empty;
                    response.SetSuccess();
                } else
                {
                    if (string.IsNullOrEmpty(model.NIKSite))
                        response.SetError("NIK Company cannot be null");

                    if (string.IsNullOrEmpty(model.FullName))
                        response.SetError("Full Name cannot be null");

                    if (string.IsNullOrEmpty(model.CompanyName))
                        response.SetError("Company Name cannot be null");
                }
            }

            if(response.IsSuccess)
            {
                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, model.NIKSite),
                    new Claim(ClaimTypes.GivenName, model.FullName),
                    new Claim("IsHRMS", model.IsHRMS.ToString())
                };

                if (!string.IsNullOrEmpty(model.CompanyName))
                    authClaims.Add(new Claim("CompanyName", model.CompanyName));

                if (!string.IsNullOrEmpty(model.Email))
                    authClaims.Add(new Claim("Email", model.Email));

                if (!string.IsNullOrEmpty(model.DepartmentName))
                    authClaims.Add(new Claim("DepartmentName", model.DepartmentName));

                if (!string.IsNullOrEmpty(model.Role))
                    authClaims.Add(new Claim("Role", model.Role));

                if (!string.IsNullOrEmpty(model.SiteCode))
                    authClaims.Add(new Claim("SiteCode", model.SiteCode));


                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));
                var token = new JwtSecurityToken(
                    issuer: _configuration["JWT:ValidIssuer"],
                    audience: _configuration["JWT:ValidAudience"],
                    expires: DateTime.Now.AddYears(3),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                    );

                model.Token = new JwtSecurityTokenHandler().WriteToken(token);

                response.ResponseObject = model;
                return Ok(response);
            }

            return Ok(response);
        }

        /// <summary>
        /// Function login ini memang dibuat untuk user login dengan menggunakan password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("authentication")]
        public IActionResult Authentication(UserLoginModel model)
        {
            var response = new ResponseModel(false);
            var userModel = new UserModel();
            Console.WriteLine(model.Username);
            if (string.IsNullOrEmpty(model.Username) && string.IsNullOrEmpty(model.Password))
            {
                response.SetError("Username & Password cant be null");
                return Ok(response);
            }

            bool IsSearchActiveDirectory = true;
            bool IsRegistered = false;
            var userlogin = _contextSM.UserLogin.FirstOrDefault(x => x.Username == model.Username);

            if (userlogin != null)
            {
                IsRegistered = true;
                if(!userlogin.IsActiveDirectory)
                    IsSearchActiveDirectory = false;
            }
            else
            {
                if (model.Username.Contains("@"))
                    IsSearchActiveDirectory = false;
            }

            if(IsSearchActiveDirectory)
            {
                #region Login Active Directory
                if(!model.Username.Contains('\\'))
                {
                    model.Username = @"JRESOURCES\" + model.Username;
                }

                var username = model.Username.Split('\\');
                DirectoryEntry _entry = new DirectoryEntry(string.Format("LDAP://{0}", username[0]), username[1], model.Password);
                DirectorySearcher _searcher = new DirectorySearcher(_entry);
                _searcher.Filter = string.Format("(samAccountName={0})", username[1]);

                try
                {
                    SearchResult _sr = _searcher.FindOne();
                    if (_sr != null)
                    {
                        userModel.Username = model.Username;
                        userModel.NIKSite = _sr.Properties["employeeid"][0].ToString();
                        userModel.FullName = _sr.Properties["name"][0].ToString();
                        userModel.Email = _sr.Properties["mail"][0].ToString();
                        userModel.SiteCode = string.Empty;
                        userModel.SiteName = string.Empty;

                        var propertyCompany = _sr.Properties["company"];
                        if (propertyCompany != null && propertyCompany.Count > 0)
                        {
                            if (propertyCompany[0] != null && !string.IsNullOrEmpty(propertyCompany[0].ToString()))
                            {
                                userModel.SiteName = propertyCompany[0].ToString();
                            }
                        }

                        UserRegisterModel userRegister = new UserRegisterModel();
                        userRegister.Username = model.Username;
                        userRegister.NIKSite = userModel.NIKSite;
                        userRegister.Email = userModel.Email;
                        userRegister.Password = string.Empty;
                        userRegister.FullName = userModel.FullName;
                        userRegister.SiteCode = userModel.SiteCode;
                        userRegister.SiteName = userModel.SiteName;
                        userRegister.CompanyName = userModel.SiteName;
                        userRegister.IsActiveDirectory = true;
                        response.SetSuccess("{0} Succes to login", userRegister.FullName);
                        if(userlogin == null)
                        {
                            Company company = _contextSM.Company.FirstOrDefault(x => x.ADCode == username[0]);
                            userRegister.SiteCode = company.CompanyName;
                            userlogin = RegisteredUser(userRegister);
                        }                        
                        response.ResponseObject = userModel;
                    } else
                    {
                        response.SetError("Username & Password Active Directory not match");
                    }
                }
                catch (Exception ex)
                {
                    //Logger.Error(LogCategory.SettingUser, "User Detail", model, ex);
                    response.SetError("Error login to active directory " + ex.Message);
                }
                #endregion
            } else
            {
                if (IsRegistered)
                {
                    if (userlogin.IsActive)
                    {
                        PasswordExtensions passwordExt = new PasswordExtensions();
                        bool Isvalid = passwordExt.VerifyHashedPassword(userlogin.PasswordHash, model.Password);
                        if (Isvalid)
                        {
                            response.SetSuccess("{0} Succes to login", userlogin.FullName);
                        }
                        else
                        {
                            response.SetError("Username & Password not match");
                        }
                    }
                    else
                    {
                        response.SetError("User {0} not active", model.Username);
                    }
                }
                else
                {
                    response.SetError("User {0} not found", model.Username);
                }
            }


            if (response.IsSuccess)
            {
                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, userlogin.NIKSite),
                    new Claim(ClaimTypes.GivenName, userlogin.FullName),
                    new Claim("IsHRMS", userlogin.IsActiveDirectory.ToString())
                };

                if (!string.IsNullOrEmpty(userlogin.SiteName))
                    authClaims.Add(new Claim("SiteName", userlogin.SiteName));

                if (!string.IsNullOrEmpty(userlogin.Email))
                    authClaims.Add(new Claim("Email", userlogin.Email));

                userModel.Username = userlogin.Username;
                userModel.Email = userlogin.Email;
                userModel.NIKSite = !string.IsNullOrEmpty(userlogin.NIKSite) ? userlogin.NIKSite : string.Empty;
                userModel.FullName = userlogin.FullName;
                userModel.SiteCode = userlogin.SiteName;


                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));
                var token = new JwtSecurityToken(
                    issuer: _configuration["JWT:ValidIssuer"],
                    audience: _configuration["JWT:ValidAudience"],
                    expires: DateTime.Now.AddYears(3),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                    );

                userModel.Token = new JwtSecurityTokenHandler().WriteToken(token);
                response.ResponseObject = userModel;
            }

            return Ok(response);
        }

        [HttpPost]
        [Route("register")]
        public IActionResult Register(UserRegisterModel model)
        {
            PasswordExtensions passwordExt = new PasswordExtensions();
            var response = new ResponseModel();

            if (ModelState.IsValid)
            {
                if (string.IsNullOrEmpty(model.Email))
                    response.SetError("Email cannot be null");

                if (string.IsNullOrEmpty(model.FullName))
                    response.SetError("Full name cannot be null");

                if (string.IsNullOrEmpty(model.SiteCode))
                    response.SetError("Site cannot be null");

                if (string.IsNullOrEmpty(model.Password) || string.IsNullOrEmpty(model.RePassword))
                {
                    response.SetError("Password dan Password confirm cannot be null");
                }
                else
                {
                    if (model.Password != model.RePassword)
                    {
                        response.SetError("Password dan Password confirm not same");
                    }
                    else
                    {
                        string errorMsg = passwordExt.ValidatiorPassword(model.Password);
                        if (!string.IsNullOrEmpty(errorMsg))
                        {
                            response.SetError(errorMsg);
                        }
                    }
                }

                if(response.IsSuccess)
                {
                    var userlogin = _contextSM.UserLogin.FirstOrDefault(x => x.Email == model.Email);
                    if(userlogin != null)
                    {
                        if (userlogin.IsDeleted)
                        {
                            response.SetError("Email {0} already rejected", model.Email);
                        } else
                        {
                            if(userlogin.IsActive)
                            {
                                response.SetError("Email {0} already register", model.Email);
                            } else
                            {
                                response.SetError("Email {0} already register and not activated", model.Email);
                            }
                        }
                    }
                }
            } else
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));

                response.SetError(message);
            }

            if(response.IsSuccess)
            {
                model.Username = model.Email;
                model.IsActiveDirectory = false;
                model.Password = passwordExt.HashPassword(model.Password);

                Company company = _contextSM.Company.FirstOrDefault(x => x.CompanyCode == model.SiteCode);
                model.SiteName = company.CompanyName;

                var userlogin = RegisteredUser(model);
                if(userlogin.Id != Guid.Empty)
                {
                    var AuthEmails = company.AuthEmail.Split(",");
                    foreach (var email in AuthEmails)
                    {
                        _emailSender.AddTo(email.Trim());
                    }

                    StringBuilder sbHTML = new StringBuilder();
                    string webRootPath = _webHostEnvironment.ContentRootPath;
                    string path = System.IO.Path.Combine(webRootPath, "EmailTemplate/");
                    string textEmail = System.IO.File.ReadAllText(path + "USER_APPROVAL.html");

                    //string approvalHash = EncryptExtension.Encrypt(company.AuthEmail + "|" + userlogin.Id.ToString());
                    string approvalHash = userlogin.Id.ToString();

                    textEmail = textEmail.Replace("{DATE_TIME}", DateTime.Now.ToString("MM/dd/yyyy hh:mm tt"));
                    textEmail = textEmail.Replace("{FULLNAME}", userlogin.FullName);
                    textEmail = textEmail.Replace("{EMAIL}", userlogin.Email);
                    textEmail = textEmail.Replace("{SITE}", userlogin.SiteName);
                    textEmail = textEmail.Replace("{URL_TARGET}", "https://ejolis.jresources.com/pica/approval/" + approvalHash);

                    _emailSender.SetSubject("PICA USER REGISTER - " + userlogin.FullName);
                    _emailSender.SetBody(textEmail);
                    _emailSender.SetBodyHTML();
                    _emailSender.SendEmail();
                    response.SetSuccess("{0} Success register", userlogin.FullName);
                } else
                {
                    response.SetError("Failed Register");
                }
            }

            return Ok(response);
        }

        [Authorize]
        [HttpGet]
        public IActionResult Current()
        {
            var user = UserModel.MappingFromAuth(User);

            return Ok(user);
        }

        [Authorize]
        [HttpPost]
        [Route("approval")]
        public IActionResult Approval(ApprovalParamModel model)
        {
            ResponseModel response = new ResponseModel();

            switch (model.ACTION)
            {
                case "APPROVE":
                    response = Approve(model.IdHash);
                    break;

                case "REJECT":
                    response = Rejected(model.IdHash);
                    break;

                default:
                    var _user = UserModel.MappingFromAuth(User);
                    Guid UserId = Guid.Empty;
                    if(Guid.TryParse(model.IdHash, out UserId))
                    {
                        var userlogin = _contextSM.UserLogin.FirstOrDefault(x => x.Id == UserId);
                        if (userlogin != null)
                        {
                            Company company = _contextSM.Company.FirstOrDefault(x => x.CompanyName == userlogin.SiteName);
                            _user = UserModel.MappingFromAuth(User);

                            if (company != null && company.AuthEmail.Contains(_user.Email))
                            {
                                if (!userlogin.IsDeleted)
                                {
                                    if (!userlogin.IsActive)
                                    {
                                        response.SetSuccess();
                                        UserModel _userApp = new UserModel();
                                        _userApp.Username = userlogin.NIKSite;
                                        _userApp.Email = userlogin.Email;
                                        _userApp.FullName = userlogin.FullName;
                                        _userApp.SiteCode = company.CompanyCode;
                                        _userApp.SiteName = company.CompanyName;
                                        _userApp.CompanyName = userlogin.VendorName;
                                        response.ResponseObject = _userApp;
                                    }
                                    else
                                    {
                                        response.SetError("User already active");
                                    }
                                }
                                else
                                {
                                    response.SetError("User already Rejected");
                                }
                            } else
                            {
                                response.SetError("Your not authrize to approve");
                            }
                        }
                        else
                        {
                            response.SetError("User not found");
                        }

                    } else
                    {
                        response.SetError("Failed to Decrypt");
                    }
                    break;
            }

            return Ok(response);
        }

        private ResponseModel Approve(string approvalhash)
        {
            ResponseModel response = new ResponseModel();
            var _user = UserModel.MappingFromAuth(User);
            Guid UserId = Guid.Empty;
            if (Guid.TryParse(approvalhash, out UserId))
            {
                var userlogin = _contextSM.UserLogin.FirstOrDefault(x => x.Id == UserId);
                if (userlogin != null)
                {
                    Company company = _contextSM.Company.FirstOrDefault(x => x.CompanyName == userlogin.SiteName);
                    if (company != null && company.AuthEmail.Contains(_user.Email))
                    {
                        if (!userlogin.IsDeleted)
                        {
                            if (!userlogin.IsActive)
                            {
                                userlogin.IsActive = true;
                                _contextSM.SaveChanges();
                                response.SetSuccess("User {0} success to activated", userlogin.FullName);
                            }
                            else
                            {
                                response.SetError("User already active");
                            }
                        }
                        else
                        {
                            response.SetError("User already Rejected");
                        }
                    }
                    else
                    {
                        response.SetError("Your not authrize to approve");
                    }
                }
                else
                {
                    response.SetError("User not found");
                }
            } else
            {
                response.SetError("Failed to Decrypt");
            }

            return response;
        }

        private ResponseModel Rejected(string approvalhash)
        {
            ResponseModel response = new ResponseModel();
            var _user = UserModel.MappingFromAuth(User);
            Guid UserId = Guid.Empty;
            if (Guid.TryParse(approvalhash, out UserId))
            {
                var userlogin = _contextSM.UserLogin.FirstOrDefault(x => x.Id == UserId);
                if (userlogin != null)
                {
                    Company company = _contextSM.Company.FirstOrDefault(x => x.CompanyName == userlogin.SiteName);
                    if (company != null && company.AuthEmail.Contains(_user.Email))
                    {
                        if (!userlogin.IsActive)
                        {
                            if (!userlogin.IsDeleted)
                            {
                                userlogin.IsDeleted = true;
                                _contextSM.SaveChanges();
                                response.SetSuccess("User {0} success to rejected", userlogin.FullName);
                            }
                            else
                            {
                                response.SetError("User already active");
                            }
                        }
                        else
                        {
                            response.SetError("User already active");
                        }
                    }
                    else
                    {
                        response.SetError("Your not authrize to rejected");
                    }
                }
                else
                {
                    response.SetError("User not found");
                }


            }
            else
            {
                response.SetError("Failed to Decrypt");
            }

            return response;
        }

        private UserLogin RegisteredUser(UserRegisterModel model)
        {
            var userLogin = new UserLogin();
            userLogin.Id = Guid.NewGuid();
            userLogin.Username = model.Username;
            userLogin.FullName = model.FullName;
            userLogin.Email = model.Email;
            userLogin.PasswordHash = model.IsActiveDirectory ? "NONE" : model.Password;
            userLogin.IsActiveDirectory = model.IsActiveDirectory;
            userLogin.NIKSite = model.NIKSite;
            userLogin.SiteCode = model.SiteCode;
            userLogin.SiteName = model.SiteName;
            userLogin.VendorName = model.CompanyName;
            userLogin.IsActive = model.IsActiveDirectory;
            userLogin.IsDeleted = false;
            userLogin.CreatedDate = DateTime.Now;
            userLogin.UpdatedDate = DateTime.Now;
            _contextSM.UserLogin.Add(userLogin);
            _contextSM.SaveChanges();

            return userLogin;
        }

    }
}