﻿using IdentityServer.DataAccess.Contracts;
using NLog;
using System;
using System.Text;

namespace IdentityServer.LoggerService
{
    public class LoggerManager : ILoggerManager
    {
        private static ILogger logger = LogManager.GetCurrentClassLogger();
        private DateTime LogTimeStart { get; set; }
        private DateTime LogTimeEnd { get; set; }
        private StringBuilder loggerTrace { get; set; }

        public void TimeStart()
        {
            LogTimeStart = DateTime.Now;
            loggerTrace = new StringBuilder();
            loggerTrace.AppendLine("");
            loggerTrace.AppendLine(string.Format("Start {0}", LogTimeStart.ToString("HH:mm:ss")));
        }

        public void TimeEnd()
        {
            LogTimeEnd = DateTime.Now;

            string timeInfo = "";
            var timeSpan = TimeSpan.FromSeconds(LogTimeEnd.Subtract(LogTimeStart).TotalSeconds);
            if (timeSpan.Hours > 0) timeInfo += "Hour : " + timeSpan.Hours + " ";
            if (timeSpan.Minutes > 0) timeInfo += "Minutes : " + timeSpan.Minutes + " ";
            if (timeSpan.Seconds > 0) timeInfo += "Seconds : " + timeSpan.Seconds;

            loggerTrace.AppendLine(string.Format("End {0}", LogTimeEnd.ToString("HH:mm:ss")));
            loggerTrace.AppendLine("Time Execution (" + (!string.IsNullOrEmpty(timeInfo) ? timeInfo : "0 seconds") + ")");

            logger.Info(loggerTrace.ToString());
        }

        public void LogAppend(string message)
        {
            loggerTrace.AppendLine(message);
        }

        public void LogAppendWithTime(string message)
        {
            loggerTrace.AppendLine(string.Format("{0} - {1}", DateTime.Now.ToString("HH:mm:ss"), message));
        }

        public void LogDebug(string message)
        {
            logger.Debug(message);
        }

        public void LogError(string message)
        {
            logger.Error(message);
        }

        public void LogInfo(string message)
        {
            logger.Info(message);
        }

        public void LogWarn(string message)
        {
            logger.Warn(message);
        }
    }
}
