namespace IdentityServer.Models
{
    public class LeaveHistoryModel
    {
        public string DateLog { get; set; }
        public string Status { get; set; }
        public string PICNik { get; set; }
        public string PICName { get; set; }
        public string PICUserRole { get; set; }
        public string PICAction { get; set; }
        public string Remarks { get; set; }
    }
}
