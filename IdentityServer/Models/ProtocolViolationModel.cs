﻿using CsvHelper.Configuration.Attributes;
using IdentityServer.Models.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Models
{
    public class ProtocolViolationModel
    {
        [Name(ProtocolViolationConstants.No)]
        public int No { get; set; }
        [Name(ProtocolViolationConstants.Site)]
        public string Site { get; set; }
        [Name(ProtocolViolationConstants.NIKSite)]
        public string NIKSite { get; set; }
        [Name(ProtocolViolationConstants.NamaPekerja)]
        public string Name { get; set; }
        [Name(ProtocolViolationConstants.Jabatan)]
        public string Position { get; set; }
        [Name(ProtocolViolationConstants.Departement)]
        public string Departement { get; set; }
        [Name(ProtocolViolationConstants.DateViolation)]
        public string DateIn { get; set; }
        [Name(ProtocolViolationConstants.IsComorbid)]
        public string IsComorbid { get; set; }
        [Name(ProtocolViolationConstants.IsCheckInNight)]
        public string IsCheckInNight { get; set; }
        [Name(ProtocolViolationConstants.IsContactPatient)]
        public string IsContactPatient { get; set; }
        [Name(ProtocolViolationConstants.IsQuarantine)]
        public string IsQuarantine { get; set; } 
        [Name(ProtocolViolationConstants.HotSeatStatus)]
        public string HotSeatStatus { get; set; }
        [Name(ProtocolViolationConstants.SelfMonitoring)]
        public string SelfMonitoring { get; set; }
        [Name(ProtocolViolationConstants.Pelanggaran)]
        public string Pelanggaran { get; set; }
        [Name(ProtocolViolationConstants.Tindakan)]
        public string Tindakan { get; set; }
    }
}