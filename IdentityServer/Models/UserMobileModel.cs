﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Models
{
    public class UserMobileModel
    {
        public string Token { get; set; }
        public string FullName { get; set; }
        public string Username { get; set; }
        public string nik { get; set; }
        public string NIKSite { get; set; }
        public string Email { get; set; }

        public string CompanyName { get; set; }
        public string DepartmentName { get; set; }
        public string Role { get; set; }
        public string KdJabat { get; set; }

        public bool IsHRMS { get; set; }

        public string SiteCode { get; set; }
        public string CostCode { get; set; }
    }

}
