﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Models
{
    public class LeaveItemModel
    {
        public string LeaveNo { get; set; }
        public string SiteCode { get; set; }
        public string CreatedDate { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string TotalLeave { get; set; }
        public string TypeLeave { get; set; }
        public string FStatus { get; set; }
    }
}
