﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Models
{
    public class LoginModel
    {
        public string NIKSite { get; set; }
        public bool IsHRMS { get; set; }
        public string FullName { get; set; }
        public string CompanyName { get; set; }
    }
}
