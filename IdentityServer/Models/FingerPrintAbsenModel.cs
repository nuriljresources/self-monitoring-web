﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Models
{
    public class FingerPrintAbsenModel
    {
        public DateTime CHECKIN_DATE { get; set; }
        public string Site { get; set; }
        public string NIKSITE { get; set; }
        public string Nama { get; set; }
        public string Jabatan { get; set; }
        public string Department { get; set; }
        public bool IsComorbid{ get; set; }
        public bool IsCheckInNight { get; set; }
        public bool IsContactPatient { get; set; }
        public bool IsQuarantine { get; set; }
    }
}
