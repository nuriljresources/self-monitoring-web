﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Models
{
    public class LeaveRequestModel
    {
        public string NIK { get; set; }
        public string Phone { get; set; }
        public string TypeLeave { get; set; }
        public DateTime DateFrom { get; set; }
        public int TotalDay { get; set; }
        public string DelegateTo { get; set; }
        public string Remarks { get; set; }
        public string DayOffId { get; set; }
    }
}
