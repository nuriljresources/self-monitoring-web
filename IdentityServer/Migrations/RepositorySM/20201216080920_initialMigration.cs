﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IdentityServer.Migrations.RepositorySM
{
    public partial class initialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SelfMonitoring",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false),
                    NIKSite = table.Column<string>(nullable: true),
                    DateCheckIn = table.Column<DateTime>(nullable: false),
                    IsEmployee = table.Column<bool>(nullable: false),
                    DateTimeMorning = table.Column<DateTime>(nullable: true),
                    DateTimeNight = table.Column<DateTime>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    SiteCode = table.Column<string>(nullable: true),
                    SiteName = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    BodyTemperature = table.Column<decimal>(nullable: true),
                    BodyTemperatureNight = table.Column<decimal>(nullable: true),
                    IsSoreThroat = table.Column<bool>(nullable: false),
                    IsFlu = table.Column<bool>(nullable: false),
                    IsFever = table.Column<bool>(nullable: false),
                    IsCough = table.Column<bool>(nullable: false),
                    IsShortness = table.Column<bool>(nullable: false),
                    IsContactPatient = table.Column<bool>(nullable: false),
                    IsQuarantine = table.Column<bool>(nullable: false),
                    PhotoMorning = table.Column<string>(nullable: true),
                    PhotoNight = table.Column<string>(nullable: true),
                    IsFromMobile = table.Column<bool>(nullable: false),
                    IsOnDuty = table.Column<bool>(nullable: false),
                    Latitude = table.Column<decimal>(nullable: true),
                    Longitude = table.Column<decimal>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    UpdatedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SelfMonitoring", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SelfMonitoring");
        }
    }
}
