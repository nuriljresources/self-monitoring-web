﻿using EmailService;
using IdentityServer.DataAccess.Contracts;
using IdentityServer.DataAccess.Entities;
using IdentityServer.DataAccess.Repository;
using IdentityServer.LoggerService;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace IdentityServer.Extensions
{
    public static class ServiceExtensions
    {
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader());
            });
        }

        public static void ConfigureIISIntegration(this IServiceCollection services)
        {
            services.Configure<IISOptions>(options =>
            {

            });
        }

        public static void ConfigureLoggerService(this IServiceCollection services)
        {
            services.AddSingleton<ILoggerManager, LoggerManager>();
        }

        public static void ConfigureSqlServerContext(this IServiceCollection services, IConfiguration config) 
        { 
            var connectionString = config.GetConnectionString("HRMSConnection");
            var connectionStringSM = config.GetConnectionString("SelfMonitoringConnection");
            services.AddEntityFrameworkSqlServer()
                .AddDbContext<RepositoryContext>(o => o.UseSqlServer(connectionString))
                .AddDbContext<RepositorySMContext>(o => o.UseSqlServer(connectionStringSM));
        }

        public static void ConfigureRepositoryWrapper(this IServiceCollection services) 
        { 
            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>(); 
        }

        public static void ConfigureEmailService(this IServiceCollection services, IConfiguration config)
        {
            var emailConfig = config.GetSection("EmailConfiguration")
                            .Get<EmailConfiguration>();
            services.AddSingleton(emailConfig);
            services.AddScoped<IEmailSender, EmailSender>();
        }


    }
}
