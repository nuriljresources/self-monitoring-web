﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace IdentityServer.DataAccess.Contracts
{
    public interface IRepositoryBase<T>
    {
        T FindById(Guid Id); 
        IQueryable<T> FindAll(); 
        IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression);
        T FindByConditionFirst(Expression<Func<T, bool>> expression);
        void Create(T entity); 
        void Update(T entity); 
        void Delete(T entity);


        bool Any(Expression<Func<T, bool>> expression);

    }
}
