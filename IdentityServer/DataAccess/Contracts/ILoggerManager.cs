﻿namespace IdentityServer.DataAccess.Contracts
{
    public interface ILoggerManager
    {
        void TimeStart();
        void TimeEnd();
        void LogAppend(string message);
        void LogAppendWithTime(string message);

        void LogInfo(string message);
        void LogWarn(string message);
        void LogDebug(string message);
        void LogError(string message);
    }
}
