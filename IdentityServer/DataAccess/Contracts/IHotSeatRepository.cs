﻿using IdentityServer.DataAccess.Entities;
using System;
using System.Collections.Generic;

namespace IdentityServer.DataAccess.Contracts
{
    public interface IHotSeatRepository : IRepositoryBase<HotSeat>
    {
    }
}
