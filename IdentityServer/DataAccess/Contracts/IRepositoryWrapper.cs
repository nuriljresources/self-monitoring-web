﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IdentityServer.DataAccess.Contracts
{
    public interface IRepositoryWrapper 
    {
        IHotSeatRepository HotSeat { get; }
        void Save(); 
    }
}
