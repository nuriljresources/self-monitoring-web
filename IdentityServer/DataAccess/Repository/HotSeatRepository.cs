﻿using IdentityServer.DataAccess.Contracts;
using IdentityServer.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IdentityServer.DataAccess.Repository
{
    public class HotSeatRepository : RepositoryBase<HotSeat>, IHotSeatRepository 
    { 
        public HotSeatRepository(RepositoryContext repositoryContext) 
            : base(repositoryContext) 
        { 
        }
    }
}
