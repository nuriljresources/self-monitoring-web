﻿using IdentityServer.DataAccess.Contracts;
using IdentityServer.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IdentityServer.DataAccess.Repository
{
    public abstract class RepositorySMBase<T> : IRepositoryBase<T> where T : class
    {
        protected RepositorySMContext RepositorySMContext { get; set; } 
        public RepositorySMBase(RepositorySMContext repositorySMContext) 
        {
            RepositorySMContext = repositorySMContext; 
        }
        
        public T FindById(Guid Id)
        {
            return RepositorySMContext.Set<T>().Find(Id);
        } 

        public IQueryable<T> FindAll() 
        { 
            return RepositorySMContext.Set<T>().AsNoTracking(); 
        } 
        
        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        { 
            return RepositorySMContext.Set<T>().Where(expression).AsNoTracking();
        }
        public T FindByConditionFirst(Expression<Func<T, bool>> expression)
        {
            return RepositorySMContext.Set<T>().FirstOrDefault(expression);
        }
        public void Create(T entity) 
        {
            RepositorySMContext.Set<T>().Add(entity); 
        } 
        
        public void Update(T entity) 
        {
            RepositorySMContext.Set<T>().Update(entity);
        } 
        
        public void Delete(T entity)
        { 
            RepositorySMContext.Set<T>().Remove(entity); 
        }
        public bool Any(Expression<Func<T, bool>> expression)
        {
            return RepositorySMContext.Set<T>().Any(expression);
        }

    }
}
