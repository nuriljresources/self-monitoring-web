﻿using IdentityServer.DataAccess.Contracts;
using IdentityServer.DataAccess.Entities;

namespace IdentityServer.DataAccess.Repository
{
    public class RepositoryWrapper : IRepositoryWrapper 
    { 
        private RepositoryContext _repoContext;
        private IHotSeatRepository _hotSeat; 

        public IHotSeatRepository HotSeat
        { 
            get 
            { 
                if (_hotSeat == null) 
                { 
                    _hotSeat = new HotSeatRepository(_repoContext); 
                } 
                return _hotSeat; 
            } 
        } 

        
        public RepositoryWrapper(RepositoryContext repositoryContext) 
        { 
            _repoContext = repositoryContext; 
        } 
        
        public void Save() 
        {
            _repoContext.SaveChanges();
        } 
    }
}
