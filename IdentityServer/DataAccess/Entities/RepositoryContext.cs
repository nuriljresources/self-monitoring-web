﻿using Microsoft.EntityFrameworkCore;

namespace IdentityServer.DataAccess.Entities
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions<RepositoryContext> options) : base(options) { }
        public DbSet<HotSeat> HotSeat { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}