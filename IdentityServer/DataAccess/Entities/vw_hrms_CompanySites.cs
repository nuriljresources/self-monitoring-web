﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.DataAccess.Entities
{
    public class vw_hrms_CompanySites
    {
        public string kdCompany { get; set; }
        public string nmCompany { get; set; }
        public string kdSite { get; set; }
        public string nmSite { get; set; }
        public string CoAddress { get; set; }
        public string Alamat { get; set; }
        public string adl { get; set; }
        public string adSt { get; set; }
        public string adPostal { get; set; }
        public string adCo { get; set; }
        public string coTelp { get; set; }
        public string coFax { get; set; }
    }
}
