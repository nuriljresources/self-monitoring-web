﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.DataAccess.Entities
{
    public class UserLogin
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public bool IsActiveDirectory { get; set; }
        public string FullName { get; set; }
        public string NIKSite { get; set; }
        public string SiteCode { get; set; }
        public string SiteName { get; set; }
        public string VendorName { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
