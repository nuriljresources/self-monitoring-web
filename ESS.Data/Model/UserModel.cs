﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ESS.Data.Model
{
    public class UserModel
    {
        public string Username { get; set; }
        public string NIKSite { get; set; }
        public string NIKCompany { get; set; }
        public bool IsHRMS { get; set; }
        public string FullName { get; set; }
        public string CompanyName { get; set; }
        public string DepartmentName { get; set; }
        public string Role { get; set; }
        public string SiteCode { get; set; }
        public string SiteName { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public static UserModel MappingFromAuth(ClaimsPrincipal user)
        {          
            UserModel _user = new UserModel();
            var TokenUser = user.Claims.FirstOrDefault(x => x.Type == "Token");
            if (TokenUser != null && TokenUser.Value != null && !string.IsNullOrEmpty(TokenUser.Value))
                _user.Token = TokenUser.Value;

            _user.NIKSite = user.Identity.Name;
            _user.FullName = user.Claims.FirstOrDefault(x => x.Type == ClaimTypes.GivenName.ToString()).Value;

            var IsHRMS = user.Claims.FirstOrDefault(x => x.Type == "IsHRMS");
            if (IsHRMS != null && IsHRMS.Value != null && !string.IsNullOrEmpty(IsHRMS.Value))
                _user.IsHRMS = bool.Parse(IsHRMS.Value);

            var CompanyName = user.Claims.FirstOrDefault(x => x.Type == "CompanyName");
            if(CompanyName != null && CompanyName.Value != null && !string.IsNullOrEmpty(CompanyName.Value))
                _user.CompanyName = CompanyName.Value;

            var Email = user.Claims.FirstOrDefault(x => x.Type == "Email");
            if (Email != null && Email.Value != null && !string.IsNullOrEmpty(Email.Value))
                _user.Email = Email.Value;

            var DepartmentName = user.Claims.FirstOrDefault(x => x.Type == "DepartmentName");
            if (DepartmentName != null && DepartmentName.Value != null && !string.IsNullOrEmpty(DepartmentName.Value))
                _user.DepartmentName = DepartmentName.Value;

            var Role = user.Claims.FirstOrDefault(x => x.Type == "Role");
            if (Role != null && Role.Value != null && !string.IsNullOrEmpty(Role.Value))
                _user.Role = Role.Value;

            var SiteCode = user.Claims.FirstOrDefault(x => x.Type == "SiteCode");
            if (SiteCode != null && SiteCode.Value != null && !string.IsNullOrEmpty(SiteCode.Value))
                _user.SiteCode = SiteCode.Value;

            return _user;
        }
    }
}
