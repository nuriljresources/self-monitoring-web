﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.Data.Model
{
    public class VaccineModel
    {
        public Guid ID { get; set; }
        //public string NIKSite { get; set; }
        public string VaccineType { get; set; }
        public DateTime VaccineDate { get; set; }
        public string VaccineDateStr { get; set; }
        public string Location { get; set; }
        public bool OnObservation { get; set; }
    }
}
