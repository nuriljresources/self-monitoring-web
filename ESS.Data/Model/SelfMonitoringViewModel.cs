﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ESS.Data.Model
{
    public class SelfMonitoringViewModel
    {
        public DateTime DateTimeCheckIn { get; set; }
        public bool IsCheckIn { get; set; } = false;
        public string NIKSite { get; set; }
        public string FullName { get; set; }
        public string Photo { get; set; }
        public string SiteName { get; set; }
        public string CompanyName { get; set; }

        public decimal BodyTemperatureMorning { get; set; }
        public decimal BodyTemperatureNight { get; set; }

        public bool IsSoreThroat { get; set; }
        public bool IsFlu { get; set; }
        public bool IsFever { get; set; }
        public bool IsCough { get; set; }
        public bool IsShortness { get; set; }
        public bool IsSmellDisturbance { get; set; }
        public bool IsTasteInterference { get; set; }
        public int SaturationO2 { get; set; }
        public bool IsOnDuty { get; set; }
        public bool IsContactPatient { get; set; }
        public bool IsQuarantine { get; set; }
        public bool IsPositiveAntigen { get; set; }
        public bool IsPositivePCR { get; set; }
        public bool IsPositiveOTG { get; set; }
        public bool IsPositiveCOV { get; set; }
        public bool IsPostVaccine { get; set; }

        // Diare
        public bool IsDiarrhea { get; set; }
        // Pegal / Nyeri ditempat suntikan
        public bool IsSorePain { get; set; }
        // Nyeri sendi dan otot
        public bool IsJointPain { get; set; }
        // Pusing
        public bool IsDizzy { get; set; }
        // Sakit Kepala
        public bool IsHeadache { get; set; }
        // Lelah
        public bool IsTired { get; set; }
        // Menggigil
        public bool IsShivering { get; set; }
        // Mual
        public bool IsNausea { get; set; }
        // Muntah
        public bool IsGag { get; set; }
        public string OtherSymptoms { get; set; }
        public bool IsUseMasker { get; set; }
        public string MaskerType { get; set; }
        public bool? IsKeepDistance { get; set; }

        public List<VaccineModel> Vaccines { get; set; }

    }
}
