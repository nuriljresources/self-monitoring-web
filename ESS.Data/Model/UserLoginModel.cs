﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ESS.Data.Model
{
    public class UserLoginModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
