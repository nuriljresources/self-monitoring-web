﻿using ESS.Data.Model;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ESS.Web.Controllers
{
    public class AuthController : Controller
    {
        private readonly IConfiguration _config;
        public AuthController(IConfiguration config)
        {
            _config = config;
        }

        [HttpGet]
        public IActionResult Login()
        {
            if(User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            UserModel model = new UserModel();
            ViewBag.Message = string.Empty;
            ViewData.Model = model;
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> Login(UserModel model)
        {
            string API_URI= _config.GetValue<string>("AppSettings:API_URI");

            var response = new ResponseModel();
            using (HttpClient client = new HttpClient())
            {
                StringContent content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                string endpoint = API_URI + "/api/auth/login";

                using (var Response = await client.PostAsync(endpoint, content))
                {
                    if (Response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var responseContent = Response.Content.ReadAsStringAsync();
                        response = JsonConvert.DeserializeObject<ResponseModel>(responseContent.Result);
                        if (response.IsSuccess)
                        {
                            model = JsonConvert.DeserializeObject<UserModel>(response.ResponseObject.ToString());
                            var authClaims = new List<Claim>
                            {
                                new Claim("Token", model.Token),
                                new Claim(ClaimTypes.Name, model.NIKSite),
                                new Claim(ClaimTypes.GivenName, model.FullName),
                                new Claim("IsHRMS", model.IsHRMS.ToString())
                            };

                            if (!string.IsNullOrEmpty(model.CompanyName))
                                authClaims.Add(new Claim("CompanyName", model.CompanyName));

                            if (!string.IsNullOrEmpty(model.Email))
                                authClaims.Add(new Claim("Email", model.Email));

                            if (!string.IsNullOrEmpty(model.DepartmentName))
                                authClaims.Add(new Claim("DepartmentName", model.DepartmentName));

                            if (!string.IsNullOrEmpty(model.Role))
                                authClaims.Add(new Claim("Role", model.Role));

                            if (!string.IsNullOrEmpty(model.SiteCode))
                                authClaims.Add(new Claim("SiteCode", model.SiteCode));

                            var userIdentity = new ClaimsIdentity(authClaims, "User Identity");
                            var userPrincipal = new ClaimsPrincipal(new[] { userIdentity });
                            await HttpContext.SignInAsync(userPrincipal);

                            response.SetSuccess(string.Format("{0} success to login", model.FullName));
                            response.ResponseObject = null;
                        } else
                        {
                            response.SetError(response.Message);
                        }
                    }
                    else
                    {
                        var responseContent = Response.Content.ReadAsStringAsync();
                        if(responseContent != null && !string.IsNullOrEmpty(responseContent.Result))
                        {
                            response.SetError(string.Format("{0} - {1}", Response.StatusCode, responseContent.Result));
                        } else
                        {
                            response.SetError(string.Format("{0} - {1}", Response.StatusCode, Response.RequestMessage));
                        }                        
                    }
                }

            }

            return Ok(response);
        }

        [HttpGet]
        public IActionResult LogOut()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Login", "Auth");
        }
    }
}
