﻿using ESS.Data.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ESS.Web.Controllers
{
    [Authorize]
    public class SelfMonitoringController : Controller
    {
        private readonly IConfiguration _config;
        private UserModel _user;
        public SelfMonitoringController(IConfiguration config)
        {
            _config = config;
        }
        public async Task<IActionResult> Index()
        {
            _user = UserModel.MappingFromAuth(User);
            string API_URI = _config.GetValue<string>("AppSettings:API_URI");
            var items = new List<SelfMonitoringViewModel>();
            var Vaccines = new List<VaccineModel>();

            Dictionary<string, string> dicChartData = new Dictionary<string, string>();
            var _listSuhuTubuh = new StringBuilder();
            var _listNoData = new StringBuilder();
            var _listSoreThroat = new StringBuilder();
            var _listFlu = new StringBuilder();
            var _listFever = new StringBuilder();
            var _listCough = new StringBuilder();
            var _listShortness = new StringBuilder();
            var _listSmellDisturbance = new StringBuilder();
            var _listTasteInterference = new StringBuilder();


            var _listIsDiarrhea = new StringBuilder();
            var _listIsSorePain = new StringBuilder();
            var _listIsJointPain = new StringBuilder();
            var _listIsDizzy = new StringBuilder();
            var _listIsHeadache = new StringBuilder();
            var _listIsTired = new StringBuilder();
            var _listIsShivering = new StringBuilder();
            var _listIsNausea = new StringBuilder();
            var _listIsGag = new StringBuilder();

            



            var _listContactPatient = new StringBuilder();
            var _listQuarantine = new StringBuilder();

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _user.Token);
                string endpoint = API_URI + "/api/self/list";

                using (var Response = await client.GetAsync(endpoint))
                {
                    if (Response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var responseContent = Response.Content.ReadAsStringAsync();
                        items = JsonConvert.DeserializeObject<List<SelfMonitoringViewModel>>(responseContent.Result);                        

                        #region Data Chart
                        _listSuhuTubuh.AppendLine("[");
                        foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
                        {
                            _listSuhuTubuh.Append("{");
                            _listSuhuTubuh.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                                , item.DateTimeCheckIn.Year
                                , (item.DateTimeCheckIn.Month-1).ToString("00")
                                , (item.DateTimeCheckIn.Day).ToString("00")
                                , (item.BodyTemperatureNight > 0 ? item.BodyTemperatureNight : item.BodyTemperatureMorning)));
                            _listSuhuTubuh.Append("},");
                            _listSuhuTubuh.AppendLine("");
                        }
                        _listSuhuTubuh.AppendLine("]");


                        _listNoData.AppendLine("[");
                        foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
                        {
                            _listNoData.Append("{");
                            _listNoData.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                                , item.DateTimeCheckIn.Year
                                , (item.DateTimeCheckIn.Month-1).ToString("00")
                                , (item.DateTimeCheckIn.Day).ToString("00")
                                , (item.IsCheckIn ? 0 : 10)));
                            _listNoData.Append("},");
                            _listNoData.AppendLine("");
                        }
                        _listNoData.AppendLine("]");




                        _listSoreThroat.AppendLine("[");
                        foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
                        {
                            _listSoreThroat.Append("{");
                            _listSoreThroat.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                                , item.DateTimeCheckIn.Year
                                , (item.DateTimeCheckIn.Month-1).ToString("00")
                                , (item.DateTimeCheckIn.Day).ToString("00")
                                , (item.IsSoreThroat ? 10 : 0)));
                            _listSoreThroat.Append("},");
                            _listSoreThroat.AppendLine("");
                        }
                        _listSoreThroat.AppendLine("]");



                        //IS FLU
                        _listFlu.AppendLine("[");
                        foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
                        {
                            _listFlu.Append("{");
                            _listFlu.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                                , item.DateTimeCheckIn.Year
                                , (item.DateTimeCheckIn.Month-1).ToString("00")
                                , (item.DateTimeCheckIn.Day).ToString("00")
                                , (item.IsFlu ? 10 : 0)));
                            _listFlu.Append("},");
                            _listFlu.AppendLine("");
                        }
                        _listFlu.AppendLine("]");


                        // Is Fever
                        _listFever.AppendLine("[");
                        foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
                        {
                            _listFever.Append("{");
                            _listFever.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                                , item.DateTimeCheckIn.Year
                                , (item.DateTimeCheckIn.Month-1).ToString("00")
                                , (item.DateTimeCheckIn.Day).ToString("00")
                                , (item.IsFever ? 10 : 0)));
                            _listFever.Append("},");
                            _listFever.AppendLine("");
                        }
                        _listFever.AppendLine("]");


                        // Cough
                        _listCough.AppendLine("[");
                        foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
                        {
                            _listCough.Append("{");
                            _listCough.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                                , item.DateTimeCheckIn.Year
                                , (item.DateTimeCheckIn.Month-1).ToString("00")
                                , (item.DateTimeCheckIn.Day).ToString("00")
                                , (item.IsCough ? 10 : 0)));
                            _listCough.Append("},");
                            _listCough.AppendLine("");
                        }
                        _listCough.AppendLine("]");


                        // Shortness
                        _listShortness.AppendLine("[");
                        foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
                        {
                            _listShortness.Append("{");
                            _listShortness.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                                , item.DateTimeCheckIn.Year
                                , (item.DateTimeCheckIn.Month-1).ToString("00")
                                , (item.DateTimeCheckIn.Day).ToString("00")
                                , (item.IsShortness ? 10 : 0)));
                            _listShortness.Append("},");
                            _listShortness.AppendLine("");
                        }
                        _listShortness.AppendLine("]");

                        // Smell Disturbance
                        _listSmellDisturbance.AppendLine("[");
                        foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
                        {
                            _listSmellDisturbance.Append("{");
                            _listSmellDisturbance.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                                , item.DateTimeCheckIn.Year
                                , (item.DateTimeCheckIn.Month - 1).ToString("00")
                                , (item.DateTimeCheckIn.Day).ToString("00")
                                , (item.IsSmellDisturbance ? 10 : 0)));
                            _listSmellDisturbance.Append("},");
                            _listSmellDisturbance.AppendLine("");
                        }
                        _listSmellDisturbance.AppendLine("]");

                        // Taste Interference
                        _listTasteInterference.AppendLine("[");
                        foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
                        {
                            _listTasteInterference.Append("{");
                            _listTasteInterference.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                                , item.DateTimeCheckIn.Year
                                , (item.DateTimeCheckIn.Month - 1).ToString("00")
                                , (item.DateTimeCheckIn.Day).ToString("00")
                                , (item.IsTasteInterference ? 10 : 0)));
                            _listTasteInterference.Append("},");
                            _listTasteInterference.AppendLine("");
                        }
                        _listTasteInterference.AppendLine("]");








                        _listIsDiarrhea.AppendLine("[");
                        foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
                        {
                            _listIsDiarrhea.Append("{");
                            _listIsDiarrhea.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                                , item.DateTimeCheckIn.Year
                                , (item.DateTimeCheckIn.Month - 1).ToString("00")
                                , (item.DateTimeCheckIn.Day).ToString("00")
                                , (item.IsDiarrhea ? 10 : 0)));
                            _listIsDiarrhea.Append("},");
                            _listIsDiarrhea.AppendLine("");
                        }
                        _listIsDiarrhea.AppendLine("]");


                        _listIsSorePain.AppendLine("[");
                        foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
                        {
                            _listIsSorePain.Append("{");
                            _listIsSorePain.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                                , item.DateTimeCheckIn.Year
                                , (item.DateTimeCheckIn.Month - 1).ToString("00")
                                , (item.DateTimeCheckIn.Day).ToString("00")
                                , (item.IsSorePain ? 10 : 0)));
                            _listIsSorePain.Append("},");
                            _listIsSorePain.AppendLine("");
                        }
                        _listIsSorePain.AppendLine("]");

                        _listIsJointPain.AppendLine("[");
                        foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
                        {
                            _listIsJointPain.Append("{");
                            _listIsJointPain.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                                , item.DateTimeCheckIn.Year
                                , (item.DateTimeCheckIn.Month - 1).ToString("00")
                                , (item.DateTimeCheckIn.Day).ToString("00")
                                , (item.IsJointPain ? 10 : 0)));
                            _listIsJointPain.Append("},");
                            _listIsJointPain.AppendLine("");
                        }
                        _listIsJointPain.AppendLine("]");



                        _listIsDizzy.AppendLine("[");
                        foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
                        {
                            _listIsDizzy.Append("{");
                            _listIsDizzy.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                                , item.DateTimeCheckIn.Year
                                , (item.DateTimeCheckIn.Month - 1).ToString("00")
                                , (item.DateTimeCheckIn.Day).ToString("00")
                                , (item.IsDizzy ? 10 : 0)));
                            _listIsDizzy.Append("},");
                            _listIsDizzy.AppendLine("");
                        }
                        _listIsDizzy.AppendLine("]");



                        _listIsHeadache.AppendLine("[");
                        foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
                        {
                            _listIsHeadache.Append("{");
                            _listIsHeadache.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                                , item.DateTimeCheckIn.Year
                                , (item.DateTimeCheckIn.Month - 1).ToString("00")
                                , (item.DateTimeCheckIn.Day).ToString("00")
                                , (item.IsHeadache ? 10 : 0)));
                            _listIsHeadache.Append("},");
                            _listIsHeadache.AppendLine("");
                        }
                        _listIsHeadache.AppendLine("]");




                        _listIsTired.AppendLine("[");
                        foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
                        {
                            _listIsTired.Append("{");
                            _listIsTired.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                                , item.DateTimeCheckIn.Year
                                , (item.DateTimeCheckIn.Month - 1).ToString("00")
                                , (item.DateTimeCheckIn.Day).ToString("00")
                                , (item.IsTired ? 10 : 0)));
                            _listIsTired.Append("},");
                            _listIsTired.AppendLine("");
                        }
                        _listIsTired.AppendLine("]");

                        _listIsShivering.AppendLine("[");
                        foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
                        {
                            _listIsShivering.Append("{");
                            _listIsShivering.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                                , item.DateTimeCheckIn.Year
                                , (item.DateTimeCheckIn.Month - 1).ToString("00")
                                , (item.DateTimeCheckIn.Day).ToString("00")
                                , (item.IsShivering ? 10 : 0)));
                            _listIsShivering.Append("},");
                            _listIsShivering.AppendLine("");
                        }
                        _listIsShivering.AppendLine("]");

                        _listIsNausea.AppendLine("[");
                        foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
                        {
                            _listIsNausea.Append("{");
                            _listIsNausea.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                                , item.DateTimeCheckIn.Year
                                , (item.DateTimeCheckIn.Month - 1).ToString("00")
                                , (item.DateTimeCheckIn.Day).ToString("00")
                                , (item.IsNausea ? 10 : 0)));
                            _listIsNausea.Append("},");
                            _listIsNausea.AppendLine("");
                        }
                        _listIsNausea.AppendLine("]");


                        _listIsGag.AppendLine("[");
                        foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
                        {
                            _listIsGag.Append("{");
                            _listIsGag.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                                , item.DateTimeCheckIn.Year
                                , (item.DateTimeCheckIn.Month - 1).ToString("00")
                                , (item.DateTimeCheckIn.Day).ToString("00")
                                , (item.IsGag ? 10 : 0)));
                            _listIsGag.Append("},");
                            _listIsGag.AppendLine("");
                        }
                        _listIsGag.AppendLine("]");




                        // ContactPatient
                        _listContactPatient.AppendLine("[");
                        foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
                        {
                            _listContactPatient.Append("{");
                            _listContactPatient.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                                , item.DateTimeCheckIn.Year
                                , (item.DateTimeCheckIn.Month-1).ToString("00")
                                , (item.DateTimeCheckIn.Day).ToString("00")
                                , (item.IsContactPatient ? 10 : 0)));
                            _listContactPatient.Append("},");
                            _listContactPatient.AppendLine("");
                        }
                        _listContactPatient.AppendLine("]");

                        // Quarantine
                        _listQuarantine.AppendLine("[");
                        foreach (var item in items.OrderBy(x => x.DateTimeCheckIn))
                        {
                            _listQuarantine.Append("{");
                            _listQuarantine.Append(string.Format(" x: new Date({0}, {1}, {2}), y: {3} "
                                , item.DateTimeCheckIn.Year
                                , (item.DateTimeCheckIn.Month-1).ToString("00")
                                , (item.DateTimeCheckIn.Day).ToString("00")
                                , (item.IsQuarantine ? 10 : 0)));
                            _listQuarantine.Append("},");
                            _listQuarantine.AppendLine("");
                        }
                        _listQuarantine.AppendLine("]");

                        #endregion                        

                    }
                }


                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _user.Token);
                endpoint = API_URI + "/api/self/vaccine";

                using (var Response = await client.GetAsync(endpoint))
                {
                    if (Response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var responseContent = Response.Content.ReadAsStringAsync();
                        Vaccines = JsonConvert.DeserializeObject<List<VaccineModel>>(responseContent.Result);
                    }
                }

            }

            dicChartData.Add("SUHU_TUBUH", _listSuhuTubuh.ToString());
            dicChartData.Add("NO_DATA", _listNoData.ToString());
            dicChartData.Add("SORETHROAT", _listSoreThroat.ToString());
            dicChartData.Add("FLU", _listFlu.ToString());
            dicChartData.Add("FEVER", _listFever.ToString());
            dicChartData.Add("COUGH", _listCough.ToString());
            dicChartData.Add("SHORTNESS", _listShortness.ToString());
            dicChartData.Add("SMELL_DISTURBANCE", _listSmellDisturbance.ToString());
            dicChartData.Add("TASTE_INTERFERENCE", _listTasteInterference.ToString());

            dicChartData.Add("DIARRHEA", _listIsDiarrhea.ToString());
            dicChartData.Add("SORE_PAIN", _listIsSorePain.ToString());
            dicChartData.Add("JOIN_PAIN", _listIsJointPain.ToString());
            dicChartData.Add("DIZZY", _listIsDizzy.ToString());
            dicChartData.Add("HEADACHE", _listIsHeadache.ToString());
            dicChartData.Add("TIRED", _listIsTired.ToString());
            dicChartData.Add("SHIVERING", _listIsShivering.ToString());
            dicChartData.Add("NAUSEA", _listIsNausea.ToString());
            dicChartData.Add("GAG", _listIsGag.ToString());

            dicChartData.Add("CONTACT_PATIENT", _listContactPatient.ToString());
            dicChartData.Add("QUARANTINE", _listQuarantine.ToString());

            ViewBag.DataChart = dicChartData;
            ViewBag.API_URI = API_URI;
            ViewBag.Vaccines = Vaccines;
            ViewData.Model = items;


            return View();
        }

        public IActionResult CheckIn()
        {
            SelfMonitoringModel model = new SelfMonitoringModel();
            string API_URI = _config.GetValue<string>("AppSettings:API_URI");

            ViewBag.API_URI = API_URI;
            ViewData.Model = model;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CheckInAsync(SelfMonitoringModel model)
        {
            string API_URI = _config.GetValue<string>("AppSettings:API_URI");

            var response = new ResponseModel();
            using (HttpClient client = new HttpClient())
            {
                StringContent content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                string endpoint = API_URI + "/api/self/check";

                using (var Response = await client.PostAsync(endpoint, content))
                {
                    if (Response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var responseContent = Response.Content.ReadAsStringAsync();
                        response = JsonConvert.DeserializeObject<ResponseModel>(responseContent.Result);
                    }
                    else
                    {
                        var responseContent = Response.Content.ReadAsStringAsync();
                        if (responseContent != null && !string.IsNullOrEmpty(responseContent.Result))
                        {
                            response.SetError(string.Format("{0} - {1}", Response.StatusCode, responseContent.Result));
                        }
                        else
                        {
                            response.SetError(string.Format("{0} - {1}", Response.StatusCode, Response.RequestMessage));
                        }
                    }
                }

            }

            return Ok(response);
        }
    }
}
