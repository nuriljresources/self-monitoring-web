﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace EmailService
{
    public interface IEmailSender
    {
        void SetSubject(string subject);
        void SetBody(string body);
        void SetBodyHTML();

        void AddTo(string email);
        void AddTo(string email, string name);
        void AddCC(string email);
        void AddCC(string email, string name);
        void AddBcc(string email);
        void AddBcc(string email, string name);
        void AddAttachment(Attachment _attachment);
        void SendEmail();
    }
}
