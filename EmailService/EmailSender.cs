﻿using System;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace EmailService
{
    public class EmailSender : IEmailSender
    {
        private readonly EmailConfiguration _emailConfig;
        private MailMessage _mailMessage;

        public string Subject { get; set; }
        public string Body { get; set; }

        public EmailSender(EmailConfiguration emailConfig)
        {
            _emailConfig = emailConfig;
            _mailMessage = new MailMessage();
        }

        public void SetSubject(string subject)
        {
            Subject = subject;
        }

        public void SetBody(string body)
        {
            Body = body;
            _mailMessage.IsBodyHtml = !string.IsNullOrEmpty(_mailMessage.Body) && _mailMessage.Body.Contains("html") ? true : false;
        }

        public void SetBodyHTML()
        {
            _mailMessage.IsBodyHtml = true;
        }


        #region Address function
        public void AddTo(string email)
        {
            _mailMessage.To.Add(email);
        }

        public void AddTo(string email, string name)
        {
            _mailMessage.To.Add(new MailAddress(email, name));
        }

        public void AddCC(string email)
        {
            _mailMessage.CC.Add(email);
        }

        public void AddCC(string email, string name)
        {
            _mailMessage.CC.Add(new MailAddress(email, name));
        }

        public void AddBcc(string email)
        {
            _mailMessage.Bcc.Add(email);
        }

        public void AddBcc(string email, string name)
        {
            _mailMessage.Bcc.Add(new MailAddress(email, name));
        }
        #endregion


        public void AddAttachment(Attachment _attachment)
        {
            _mailMessage.Attachments.Add(_attachment);
        }

        public void SendEmail()
        {
            _mailMessage.From = new MailAddress(_emailConfig.From);
            _mailMessage.Subject = Subject;
            _mailMessage.Body = Body;
            using (var client = new SmtpClient())
            {
                try
                {
                    client.Host = _emailConfig.SmtpServer;
                    client.Port = _emailConfig.Port;
                    client.EnableSsl = true;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.Credentials = new System.Net.NetworkCredential(_emailConfig.UserName, _emailConfig.Password);
                    client.Send(_mailMessage);
                }
                catch
                {
                    //log an error message or throw an exception, or both.
                    throw;
                }
                finally
                {
                    client.Dispose();
                }
            }
        }


    }
}
