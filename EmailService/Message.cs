﻿using System.Collections.Generic;
using System.Net.Mail;

namespace EmailService
{
    public class Message
    {
        public List<MailAddress> To { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }

        public AttachmentCollection Attachments { get; set; }

        public Message(List<MailAddress> to, string subject, string content, AttachmentCollection attachments)
        {
            To = to;
            Subject = subject;
            Content = content;
            Attachments = attachments;
        }
    }
}
